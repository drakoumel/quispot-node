var AM = require('../modules/account-manager');
var passport = require('passport');


exports.login = function (req, res) {
    console.log(req.body);
    if (req.body.lguser.username !== undefined && req.body.lguser.password !== undefined) {
        console.log("login - will try loginByCredentials");
        AM.loginByCredentials(req.body.lguser.username, req.body.lguser.password, function (o) {
            if (o != -1 && o > 0) {
                req.session.loggedIn = true;
                req.session.authorized = true;
                console.log("1.1 - sucess");
                res.cookie('user', req.body.lguser.username, { signed: true })
                res.cookie('watcher', o, { signed: true })
                res.redirect('/');
            }
            else if (o == -1) {
                req.session.loggedIn = false;
                req.session.authorized = false;
                req.session.lastPage = '/';
                console.log("1.3 - login credentials incorrect");
                res.write('login credentials incorrect');
                res.end();
            }
        });
    }
}

exports.logout = function (req, res) {
    console.log(req.body);
    if (req.session.loggedIn == true)
    { req.session.destroy(); }
    res.render('login', { title: 'Please log in.' });
}

exports.index = function (req, res) {
    if (req.session.loggedIn && req.signedCookies.user !== undefined) {
        req.session.user = 0;
        req.session.lastPage = '/index';
        res.render('index', { title: 'Express' });
    }
    else {
        req.session.lastPage = '/';
        res.render('login', { title: 'Please log in.' });
    }

};


