exports.category = function (req, res) {
    var category = require('../models/category');
    category.find({}, function (err, category) {
        if (err)
            console.log(err);

        if (!category)
            console.log('no cate');

        if (category) {
            res.render('create.ejs', {
                user: req.user,
                category: category
            });
        }
    })
}

exports.spots = function (req, res) {
    var spot = require('../models/spot');
    console.log(req.body);
    if (req.body.mainactivity == 's') {
        spot.find({ "sports": { $in: [req.body.activity] } }, function (err, spot) {
            if (err || !spot)
                res.end(JSON.stringify({ result: "error" }));

            if (spot) {
                res.end(JSON.stringify({ result: spot }));
            }
        })
    }
        //if cardgame
    else if (req.body.mainactivity == 'c') {
        spot.find({ "cardgames": { $in: [req.body.activity] } }, function (err, spot) {
            if (err || !spot)
                res.end(JSON.stringify({ result: "error" }));

            if (spot) {
                res.end(JSON.stringify({ result: spot }));
            }
        })
    }
        //if board game
    else if (req.body.mainactivity == 'b') {
        spot.find({ "boardgames": { $in: [req.body.activity] } }, function (err, spot) {
            if (err || !spot)
                res.end(JSON.stringify({ result: "error" }));

            if (spot) {
                res.end(JSON.stringify({ result: spot }));
            }
        })
    }
}

exports.createEvent = function (req, res) {
    var Event = require('../models/event');
    var newEvent = new Event();
    console.log(req.body);
    newEvent.event.SubCategory = req.body.SubCategory;
    newEvent.event.SpotID = req.body.SpotID;
    newEvent.event.CreatorID = req.user._id;
    newEvent.event.AdminID = req.user._id;
    newEvent.event.When = req.body.date;
    newEvent.event.CreateAt = null;
    newEvent.event.HowMany = req.body.HowMany;
    newEvent.event.Invites = null;
    newEvent.event.HowManyLeft = 0;

    newEvent.save(function (err) {
        if (err)
            throw err;
        console.log('we have an event!');
        console.log(newEvent);
        res.render('event.ejs', {
            user: req.user,
            event: newEvent
        });
        req.url = '/';
    });
}

exports.test = function (req, res) {
    var Event = require('../models/event');

    Event.find({ "_id": "5314b79a8feb739415e08477" }, function (err, event) {
        if (err || !event)
            res.end(JSON.stringify({ result: "error" }));

        if (event) {
            res.render('event.ejs', {
                user: req.user,
                event: event[0]
            });
            req.url = '/';
        }
    });
}