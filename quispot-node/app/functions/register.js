var AM = require('../modules/account-manager');


exports.register = function (req, res) {
    res.render('register', { title: 'Register' });
}

exports.registerDetails = function (req, res) {
    if (req.body.rguser.password === req.body.rguser.retypepassword) {

        AM.registerUser(req.body.rguser.username, req.body.rguser.password, req.body.rguser.dob, req.body.rguser.fn, req.body.rguser.mn, req.body.rguser.ln, req.body.rguser.email, function (o) {
            if (0 != -1 && o > 0) {
                res.redirect('/');
            }
            else if (0 == -1) {
                res.redirect('/register');
                res.write('register details already exist');
                res.end();
            }
        });
    }
}