exports.init = function (req, res) {
    var category = require('../models/category');
    category.find({}, function (err, category) {
        if (err)
            console.log(err);

        if (!category)
            console.log('no cate');

        if (category) {
            res.render('search.ejs', {
                user: req.user,
                category: category
            });
        }
    })
}

exports.spots = function (req, res) {
    var spot = require('../models/spot');
    console.log(req.body);
    if (req.body.mainactivity == 's') {
        spot.find({ "sports": { $in: [req.body.activity] } }, function (err, spot) {
            if (err || !spot)
                res.end(JSON.stringify({ result: "error" }));

            if (spot) {
                res.end(JSON.stringify({ result: spot }));
            }
        })
    }
        //if cardgame
    else if (req.body.mainactivity == 'c') {
        spot.find({ "cardgames": { $in: [req.body.activity] } }, function (err, spot) {
            if (err || !spot)
                res.end(JSON.stringify({ result: "error" }));

            if (spot) {
                res.end(JSON.stringify({ result: spot }));
            }
        })
    }
        //if board game
    else if (req.body.mainactivity == 'b') {
        spot.find({ "boardgames": { $in: [req.body.activity] } }, function (err, spot) {
            if (err || !spot)
                res.end(JSON.stringify({ result: "error" }));

            if (spot) {
                res.end(JSON.stringify({ result: spot }));
            }
        })
    }
}

