exports.eventDetails = function (req, res) {
        var pg = require('pg');
        var localconstring = "postgres://admin:cswow2010@37.247.55.226:5432/totum";

        var client = new pg.Client(localconstring);
        client.on('drain', client.end.bind(client)); //disconnect client when all queries are finished
        client.connect(function (err, client, done) {

            var data = [];
            prepStatement = {
                name: 'select event details based on id',
                text: 'SELECT * FROM event_details where id = $1',
                values: [req.body.id]
            };
            var query = client.query(prepStatement);

            query.on('row', function (result) {
                
                var dataItem = {
                    EventName: result.name, PrivacyLevel: result.privacyid, TypeInt: result.eventtypeid, Lat: result.lat,
                    Lon: result.lon, ExpectedDate: result.dateexpected, Desc: result.description, Down: result.downvote,
                    Up: result.upvote, PostCode: result.postcode, Address: result.address, Marketed: result.marketed,
                    CategoryID: result.categoryid, PublisherID: result.publisherid
                };
                console.log(dataItem);
                data.push(dataItem);
            });

            query.on('error', function (err) {
                console.log(err);
                res.write(JSON.stringify({ error: err }));
            });

            query.on('end', function () { res.end(JSON.stringify( data )); });
        });
};


exports.Events = function (req, res) {
        var pg = require('pg');
        var localconstring = "postgres://admin:cswow2010@37.247.55.226:5432/totum";

        var client = new pg.Client(localconstring);
        client.on('drain', client.end.bind(client)); //disconnect client when all queries are finished
        client.connect(function (err, client, done) {

            var data = [];
            var query = client.query('SELECT id from public.event');

            query.on('row', function (result) {
                console.log("events" + result.id);
                data.push(result.id);
            });

            query.on('error', function (err) {
                console.log(err);
                res.write(JSON.stringify({ error: err }));
            });

            query.on('end', function () { res.end(JSON.stringify({ data: data })); });
        });
}

exports.registerEvent = function (req, res) {
        var pg = require('pg');
        var localconstring = "postgres://admin:cswow2010@37.247.55.226:5432/totum";
        queryCount = 0;

        var client = new pg.Client(localconstring);
        client.on('drain', client.end.bind(client)); //disconnect client when all queries are finished
        client.connect(function (err, client, done) {

            prepStatement = {
                name: 'create event',
                text: 'SELECT create_event($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12)',
                values: [req.body.TypeID, req.body.EventName, req.body.Marketed, req.body.Lat, req.body.Lon, req.body.Privacy, req.body.DateStamp
                        ,req.body.Address,req.body.PostCode,req.body.Desc,req.body.Publisher,req.body.CategoryID]
            };

            client.query(prepStatement, function (err, result) {
                if (err) {
                    console.log("error hit herer "+err);
                }

                if (result.rowCount > 0)
                {
                    client.end();
                    res.end(JSON.stringify({ eventid: result.rows[0] }));
                }
            })
        });
}

exports.updateEvent = function (req, res) {
        var pg = require('pg');
        var localconstring = "postgres://admin:cswow2010@37.247.55.226:5432/totum";
        queryCount = 0;

        var client = new pg.Client(localconstring);
        client.on('drain', client.end.bind(client)); //disconnect client when all queries are finished
        client.connect(function (err, client, done) {

            prepStatement = {
                name: 'update event',
                text: 'SELECT update_event($1,$2,$3,$4,$5,$6,$7,$8)',
                values: [req.body.ID,req.body.TypeID, req.body.EventName,1, req.body.DateStamp,req.body.Address, req.body.Desc, req.body.CategoryID]
            };

            console.log(prepStatement);

            client.query(prepStatement, function (err, result) {
                if (err) {
                    console.log("error hit herer " + err);
                }

                if (result.rowCount > 0) {
                    client.end();
                    console.log(result);
                    res.end(JSON.stringify({ success: result }));
                }
            })
        });
}

exports.categoryDetails = function (req, res) {
        var pg = require('pg');
        var localconstring = "postgres://admin:cswow2010@37.247.55.226:5432/totum";

        var client = new pg.Client(localconstring);
        client.on('drain', client.end.bind(client)); //disconnect client when all queries are finished
        client.connect(function (err, client, done) {

            var data = [];
            var query = client.query('SELECT  * from public.category');

            query.on('row', function (result) {
                data.push(result);
            });

            query.on('error', function (err) {
                console.log(err);
                res.write(JSON.stringify({ error: err }));
            });

            query.on('end', function () { res.end(JSON.stringify({ data: data })); });
        });
}

exports.eventtypeDetails = function (req, res) {
        var pg = require('pg');
        var localconstring = "postgres://admin:cswow2010@37.247.55.226:5432/totum";

        var client = new pg.Client(localconstring);
        client.on('drain', client.end.bind(client)); //disconnect client when all queries are finished
        client.connect(function (err, client, done) {

            var data = [];
            var query = client.query('SELECT  * from public.eventtype');

            query.on('row', function (result) {
                console.log(result);
                data.push(result);
            });

            query.on('error', function (err) {
                console.log(err);
                res.write(JSON.stringify({ error: err }));
            });

            query.on('end', function () { res.end(JSON.stringify({ data: data })); });
        });
}