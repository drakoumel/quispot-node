// load the things we need
var mongoose = require('mongoose');

// define the schema for our event model
var eventSchema = mongoose.Schema({

    event: {
        SubCategory: String,
        SpotID: String,
        CreatorID : String,
        AdminID : String,
        When: { type: Date, default: Date.now },
        CreatedAt: { type: Date, default: Date.now },
        HowMany: String,
        HowManyLeft: String,
        Invites : Array
    }
});


module.exports = mongoose.model('Event', eventSchema);