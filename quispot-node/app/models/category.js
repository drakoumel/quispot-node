// load the things we need
var mongoose = require('mongoose');

// define the schema for our category model
var categorySchema = mongoose.Schema(

    {
        "name" : String,
        "children" : Array
    }
, { collection: 'category' }
);


module.exports = mongoose.model('category', categorySchema);