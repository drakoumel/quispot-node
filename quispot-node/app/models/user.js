// load the things we need
var mongoose = require('mongoose');

// define the schema for our user model
var userSchema = mongoose.Schema({

    local: {
        username     : String,
        FN           : String,
        LN           : String,
        email        : String,
        password     : String,
        salt         : String,
        City         : String,
        Age          : [Number],
        Address      : String,
        Newsletters  : Boolean

    },
    facebook         : {
        id           : String,
        token        : String,
        email        : String,
        name         : String
    },
    twitter          : {
        id           : String,
        token        : String,
        displayName  : String,
        username     : String
    },
    google           : {
        id           : String,
        token        : String,
        email        : String,
        name         : String
    }

});


module.exports = mongoose.model('User', userSchema);