// load the things we need
var mongoose = require('mongoose');

// define the schema for our category model
var spotSchema = mongoose.Schema(

    {
        "id" : String,
        "name":String,
        "FN": String,
        "LN": String,
        "lat": String,
        "lon": String,
        "Address": String,
        "PostCode": String,
        "email": String,
        "mobile": String,
        "landline": String,
        "fax": String,
        "sports":Array,
        "cardgames":Array,
        "boardgames":Array    
    }
, { collection: 'spot' }
);


module.exports = mongoose.model('spot', spotSchema);