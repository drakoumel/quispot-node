﻿searchManager = null;
cords = null;

//-- Map
mapleft = 0;
maptop = 0;
map = null;
geoLocationProvider = null;
var TILE_SIZE = 256;

//-- Main pin for event allocation
mainDragablePin = null;
mainPushpinOptions = { icon: '../images/PinIt.png', width: 60, height: 60, draggable: true };
mainPushPinDockX = 500;
mainPushPinDockY = 10;
mainPushPinDockLatitude = null;
mainPushPinDockLongitude = null;
bldragOn = false;
blDragEnded = false;

//-- Mouse
mouseUp = null;
mouseUpDragStopLat = null;
mouseUpDragStopLon = null;


arrayEvent = new Array();
arrayCategoryOptions = null;
arrayEventType = null;
lastEventID = 0;
infowindowZindex = 1;
lastclickmilisecs = 0;
//===============================================================================
//=================== SETUP =====================================================
//===============================================================================

function init() {
    IExplorerFix();

    // Main map options and Map initialization
    var mapOptions = {
        zoom: 10,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        minZoom: 3,
        maxZoom: 23,
        disableDefaultUI: true,
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.BIG,
            position: google.maps.ControlPosition.LEFT_BOTTOM
        }
    };

    //this.addNewPin = function () { console.log(this.map); };

    map = new google.maps.Map(document.getElementById("GoogleMap"), this.mapOptions);

        setEnvironment();
    
}

function IExplorerFix() {
    if (!window.location.origin) {
        window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
    }
}

function setEnvironment() {


    profileTab();

    infoSpammer();

    adjustInfoWinodw();

    $(document).dblclick(function (event) {
        if ($(event.target).attr('class') !== undefined) {
            if ($(event.target).attr('class').indexOf("transformable") !== -1)
            { ReplaceByInput($(event.target)); }
        }
    });

    CategoryDetails();
    EventTypeDetails();
    //var input = document.getElementById('searchTextField');
    //var autocomplete = new google.maps.places.Autocomplete(input, {
    //    types: ["geocode"]
    //});
    //autocomplete.bindTo('bounds', map);
    //var place = autocomplete.getPlace();
    //if (place.geometry.viewport) {
    //    map.fitBounds(place.geometry.viewport);
    //} else {
    //    map.setCenter(place.geometry.location);
    //    map.setZoom(17);
    //}


}

function RequestData() {
    $.ajax({
        url: window.location.origin + '/events',
        contentType: 'application/json: charset=utf-8',
        dataType: 'json',
        type: 'GET',
        cache: false,
        timeout: 5000,
        async: true,
        success: function (result) {
            result.data.forEach(function (id) { initEvent(new Event(id)); setMaxEventID(id); });
            return true;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('error ' + textStatus + " " + errorThrown);
            return false;
        }
    });
}
//===============================================================================
//=================== Environment Functions =====================================
//===============================================================================


function profileTab() {
    $(".lblAction,.profImg,.action,.settings-panel,#Toolkit").hover(function () { SettingsPanelToggle(); });
}

function SettingsPanelToggle() {
    if (!$(".settings-panel").is(":visible")) {
        $(".settings-panel").fadeIn(0);
    }
    else {
        if ($(".settings-panel:hover").length == 0 && $(".lblAction:hover").length == 0 && $(".action:hover").length == 0 && $(".profImg:hover").length == 0 && $("#Toolkit:hover").length == 0) {
            $(".settings-panel").fadeOut(0);
        }
    }
}

function infoSpammer() {
    $(".spammer").resizable({
        handles: 's'
    });
}

function ReplaceByInput(clickedElement) {
    //find clicked element val / inner html
    var clickedElementValue = null;
    if ($(clickedElement).val() != "" && $(clickedElement).val() != undefined) {
        clickedElementValue = $(clickedElement).val();
    }
    else if ($(clickedElement).html() != "" && $(clickedElement).html() != undefined) {
        clickedElementValue = $(clickedElement).html();
    }

    // store the new input element
    var input = $('<input type="text"></input>').addClass("replaceInput");
    $(input).val(clickedElementValue);
    $(clickedElement).replaceWith(input);

    // set cursor and focus to input element
    input[0].selectionStart = input[0].selectionEnd = input.val().length;
    $(input).focus();

    var rereplace = function () {
        $(input).on("blur", function () {
            $(clickedElement).html($(input).val());
            $(input).replaceWith(clickedElement);
        });
    }

    //safety net!
    setTimeout(rereplace, 100);
}


//===============================================================================
//=================== On Click Functions ========================================
//===============================================================================

function setMaxEventID(id) {
    if (id > lastEventID)
    { lastEventID = id; }
}

function CategoryDetails() {
    var success = false;
    $.ajax({
        type: "GET",
        url: window.location.origin + '/categoryDetails',
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (result) {
            arrayCategoryOptions = result.data;
            success = true;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('error ' + textStatus + " " + errorThrown);
            success = false;
        }
    });
    return success;
}

function EventTypeDetails() {
    var success = false;
    $.ajax({
        type: "GET",
        url: window.location.origin + '/eventtypeDetails',
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (result) {
            arrayEventType = result.data;
            success = true;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('error ' + textStatus + " " + errorThrown);
            success = false;
        }
    });
    return success;
}

function adjustInfoWinodw() {
    google.maps.InfoWindow.prototype._open = google.maps.InfoWindow.prototype.open;
    google.maps.InfoWindow.prototype._close = google.maps.InfoWindow.prototype.close;
    google.maps.InfoWindow.prototype._openedState = false;

    google.maps.InfoWindow.prototype.open =
    function (map, anchor, ID) {
        this._open(map, anchor);
        $.event.trigger({
            type: "InfoWinodwOpen" + ID,
        });
        this._openedState = true;
    };

    google.maps.InfoWindow.prototype.close =
        function (ID) {
            this._close();
            this._openedState = false;
        };

    google.maps.InfoWindow.prototype.getOpenedState =
        function () {
            return this._openedState;
        };

    google.maps.InfoWindow.prototype.setOpenedState =
        function (val) {
            this._openedState = val;
        };
}
