﻿searchManager = null;
cords = null;

//-- Map
mapleft = 0;
maptop = 0;
map = null;
geoLocationProvider = null;
var TILE_SIZE = 256;

//-- Main pin for event allocation
mainDragablePin = null;
mainPushpinOptions = { icon: '../images/PinIt.png', width: 60, height: 60, draggable: true };
mainPushPinDockX = 500;
mainPushPinDockY = 10;
mainPushPinDockLatitude = null;
mainPushPinDockLongitude = null;
bldragOn = false;
blDragEnded = false;

//-- Mouse
mouseUp = null;
mouseUpDragStopLat = null;
mouseUpDragStopLon = null;

function init() {
    GetMap();
    profileTab();
    spammer();

    $(document).dblclick(function (event) {
        if ($(event.target).attr('class') === 'transformable')
        { ReplaceByInput($(event.target)); }
    });
}

function GetMap() {
    mapOptions = {
        zoom: 10,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        minZoom: 3,
        maxZoom: 23,
        disableDefaultUI: true,
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.BIG,
            position: google.maps.ControlPosition.LEFT_BOTTOM
        }

    };

    map = new google.maps.Map(document.getElementById("GoogleMap"), mapOptions);
    mapMouseUpEventHandler();
    getGeolocationUser();
    google.maps.event.addListener(map, 'rightclick', rightclick);
    GatherEvents();
}

rightclick = function (e) {
    addNewPin(e.latLng);
}

function GatherEvents() {
    $.ajax({
        url: window.location.origin + '/events',
        contentType: 'application/json: charset=utf-8',
        dataType: 'json',
        type: 'GET',
        cache: false,
        timeout: 5000,
        async: false,
        success: function (result) {
            result.data.forEach(function (o) { addExistingPin(o); });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('error ' + textStatus + " " + errorThrown);
        }
    });
}

function addExistingPin(Object) {

    var e = null;
    if (typeof Object.IsEvent === 'function')
    { console.log("exists"); e = Object; }
    else
    {
        e = new Event(Object.ID, Object.EventName, Object.PrivacyLevel, Object.Lat, Object.Lon, Object.ExpectedDate,
                     Object.TypeInt, Object.Address, Object.PostCode, Object.Desc, Object.Up, Object.Down);
    }

    google.maps.InfoWindow.prototype.toOpen = true;

    var infowindow = new google.maps.InfoWindow({
        content: ExistingEventBoxHTML(e),
        maxWidth: 500
    });

    var marker = new google.maps.Marker({
        map: map,
        draggable: false,
        position: new google.maps.LatLng(e.getLat(), e.getLon()),
        icon: '../images/Pins/StarBlue.png'
    });

    google.maps.event.addListener(marker, 'click', function () {
        if (infowindow.toOpen) {
            infowindow.open(map, marker);
            infowindow.toOpen = false;
        }
        else if (!infowindow.toOpen) {
            var newEventData = null // add code for gathering data from infowindow!
            if (Save(e, newEventData)) {
                infowindow.close();
                infowindow.toOpen = true;
            }
        }
    });
}

function ExistingEventBoxHTML(Event) {

    var wrapper = $('<div></div>');
    var div = $('<div></div>').addClass("event");
    var deailsDiv = $('<div></div>').addClass("details");
    var extraDeailsDiv = $('<div></div>').addClass("details extra");

    var ul = $('<ul></ul>');
    var li = $('<li></li>');

    deailsDiv.append($('<span class="vote up">D</span>'));
    deailsDiv.append($('<span class="vote down">U</span>'));

    ul.append($('<li></li>').append($('<h6>' + Event.getEventName() + '</h6>').addClass("transformable")));
    ul.append($('<li></li>').append($('<label>' + Event.getEventDate() + '</label>').addClass("transformable")));
    ul.append($('<li></li>').append($('<p>' + Event.getDescription() + '</p>').addClass("transformable")));
    ul.append($('<li></li>').append($('<span>Type</span>')));
    ul.append($('<li></li>').append($('<span>Privacy</span>')));
    ul.append($('<li></li>').append($('<label>Address:</label><label>' + Event.getAddress() + '</label>').addClass("transformable")));
    ul.append($('<li></li>').append($('<label>PostCode:</label><label>' + Event.getPostCode() + '</label>').addClass("transformable")));

    deailsDiv.append(ul);
    div.append(deailsDiv);
    wrapper.append(div);

    return wrapper.html();
}


function addNewPin(cords) {
    map.setCenter(cords);
    console.log(cords);

    var newID = GenerateNewID();
    var contentString = NewEventBoxHTML(newID);

    infowindow = new google.maps.InfoWindow({
        content: contentString,
        maxHeight: 500
    });

    var marker = new google.maps.Marker({
        map: map,
        draggable: false,
        position: new google.maps.LatLng(cords.lat(), cords.lng()),
        icon: '../images/Pins/StarBlue.png'
    });
    google.maps.event.addListener(marker, 'click', function () {
        marker.setAnimation(google.maps.Animation.BOUNCE);
        console.log(cords.lat());
        var e = new Event('',
                          $("#EventName" + newID).val(),
                          1,
                          cords.lat(),
                          cords.lng(),
                          $("#EventDate" + newID).val(),
                          1,
                          '',
                          '',
                          $("#EventDesc" + newID).val(),
                          0,
                          0);

        if (Save(null, e))
        { addExistingPin(e); marker.setAnimation(null); infowindow.close(); marker.setMap(null);}
    });
    setTimeout(function () { infowindow.open(map, marker); }, 100);
}

function GenerateNewID() {
    return 123;
}


function NewEventBoxHTML(newID) {

    var wrapper = $('<div></div>');
    var div = $('<div></div>').addClass("event new");
    var deailsDiv = $('<div></div>').addClass("details");
    var extraDeailsDiv = $('<div></div>').addClass("details extra");

    var ul = $('<ul onclic="ReplaceByInput(this)"></ul>');
    var li = $('<li></li>');

    deailsDiv.append($('<span class="vote up">D</span>'));
    deailsDiv.append($('<span class="vote down">U</span>'));

    ul.append($('<li></li>').append($('<h6>' + 'Enter Name' + '</h6>').addClass("transformable")));
    ul.append($('<li></li>').append($('<input type="inpu"></input>').attr("id", "EventName" + newID)));
    ul.append($('<li></li>').append($('<label>' + 'Enter Date' + '</label>').addClass("non-transformable")));
    ul.append($('<li></li>').append($('<input type="inpu"></input>').attr("id", "EventDate" + newID)));
    ul.append($('<li></li>').append($('<p>' + 'Enter Description' + '</p>').addClass("non-transformable")));
    ul.append($('<li></li>').append($('<input type="inpu"></input>').attr("id", "EventDesc" + newID)));
    ul.append($('<li></li>').append($('<span>Type</span>')));
    ul.append($('<li></li>').append($('<span>Privacy</span>')));
    ul.append($('<li></li>').append($('<label>Address:</label><label>' + 'this will be calced' + '</label>').addClass("transformable")));
    ul.append($('<li></li>').append($('<label>PostCode:</label><label>' + 'this will be calced' + '</label>').addClass("transformable")));

    deailsDiv.append(ul);
    div.append(deailsDiv);
    wrapper.append(div);

    return wrapper.html();
}

function ReplaceByInput(clickedElement) {
    console.log("test");
    var input = $('<input type="text"></input>');
    $(clickedElement).replaceWith(input);
    input[0].selectionStart = input[0].selectionEnd = input.val().length;
    $(input).focus();

    var rereplace = function () {
        $(input).on("blur", function () {

            $(input).replaceWith(clickedElement);
            console.log("return back to element" + clickedElement);
        });
    }

    setTimeout(rereplace, 100);
}

function ToggleTypBtn(element) {
    other = $(element).siblings('span');
    other.push(element);

    $.each(other, function (element) { console.log(element) });
}

mouseUpCords = function (e) {
    console.log('mousemove:' + e.latLng.lat() + ':' + e.latLng.lng());
}

function mapMouseUpEventHandler() {
    //mouseUp = google.maps.event.addListener(map, 'mousemove', mouseUpCords);
}

function Event(ID, EventName, PrivacyLevel, Lat, Lon, ExpectedDate, TypeInt, Address, PostCode, Desc, Up, Down) {
    var _id,
        _name,
        _privacy,
        _latitude,
        _longitude,
        _datestamp,
        _typeid,
        _address,
        _postcode,
        _description,
        _upvotes,
        _downvotes;

    this._id = ID;
    this._name = EventName;
    this._privacy = PrivacyLevel;
    this._latitude = Lat;
    this._longitude = Lon;
    this._datestamp = ExpectedDate;
    this._typeid = TypeInt;
    this._address = Address;
    this._postcode = PostCode;
    this._description = Desc;
    this._upvotes = Up;
    this._downvotes = Down;

    this.IsEvent = function () { return true; }

    this.getEventID = function () { return this._id; }
    this.getEventName = function () { return this._name; }
    this.getPrivacyInt = function () { return this._privacy; }
    this.getLat = function () { return this._latitude; }
    this.getLon = function () { return this._longitude; }
    this.getEventDate = function () { return this._datestamp; }
    this.getTypeInt = function () { return this._typeid; }
    this.getAddress = function () { return this._address; }
    this.getPostCode = function () { return this._postcode; }
    this.getDescription = function () { return this._description; }
    this.getUpVoteInt = function () { return this._upvotes; }
    this.getDownVoteInt = function () { return this._downvotes; }

    this.setEventName = function (nValue) { this._name = nValue; }
    this.setPrivacyInt = function (nValue) { this._privacy = nValue; }
    this.setLat = function (nValue) { this._latitude = nValue; }
    this.setLon = function (nValue) { this._longitude = nValue; }
    this.setEventDate = function (nValue) { this._datestamp = nValue; }
    this.setTypeInt = function (nValue) { this._typeid = nValue; }
    this.setAddress = function (nValue) { this._address = nValue; }
    this.setPostCode = function (nValue) { this._postcode = nValue; }
    this.setDrescription = function (nValue) { this._description = nValue; }
    this.setUpVote = function (nValue) { this._upvotes = nValue; }
    this.setDownVote = function (nValue) { this._downvotes = nValue; }

    this.toJSONString = function () {
        return {
            EventName: this._name, PrivacyLevel: this._privacy, TypeInt: this._typeid, Up: this._upvotes, Lat: this._latitude,
            Lon: this._longitude, ExpectedDate: this._datestamp, Desc: this._description, Down: this._downvotes,
            PostCode: this._postcode, Address: this._address, ID: this._id
        }
    }
}

function Save(oldEv, newEv) {
    var success = false;

    if (oldEv === null) { // IF old is nothing then create a new one!
        console.log("New Event");
        console.log(newEv.toJSONString());
        $.ajax({
            type: "POST",
            url: window.location.origin + '/registerEvent',
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            data: JSON.stringify(newEv.toJSONString()),
            success: function (result) {
                success = true;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('error ' + textStatus + " " + errorThrown);
                success = false;
            }
        });
    }
    else {
        console.log("Old Event");
        console.log("compare " + oldEv + "with " + newEv);
        success = true;
    }
    console.log("ajax end");
    console.log(success);
    return success;
}

function getGeolocationUser() {
    // Try W3C Geolocation (Preferred)
    if (navigator.geolocation) {
        browserSupportFlag = true;
        navigator.geolocation.getCurrentPosition(function (position) {
            initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            console.log(position);
            map.setCenter(initialLocation);
        }, function () {
            handleNoGeolocation(browserSupportFlag);
        });
    }
        // Browser doesn't support Geolocation
    else {
        browserSupportFlag = false;
        handleNoGeolocation(browserSupportFlag);
    }

    function handleNoGeolocation(errorFlag) {
        if (errorFlag == true) {
            alert("Geolocation service failed.");
            initialLocation = newyork;
        } else {
            alert("Your browser doesn't support geolocation. We've placed you in Siberia.");
            initialLocation = siberia;
        }
        map.setCenter(initialLocation);
    }
}

function profileTab() {
    $(".lblAction,.profImg,.action,.settings-panel,#Toolkit").hover(function () { SettingsPanelToggle(); });
}

function SettingsPanelToggle() {
    if (!$(".settings-panel").is(":visible")) {
        $(".settings-panel").fadeIn(0);
    }
    else {
        if ($(".settings-panel:hover").length == 0 && $(".lblAction:hover").length == 0 && $(".action:hover").length == 0 && $(".profImg:hover").length == 0 && $("#Toolkit:hover").length == 0) {
            $(".settings-panel").fadeOut(0);
        }
    }
}

function Action(Element) {
    var htmlForModal = "<div></div>";
    switch ($(Element).attr("id")) {
        case "1": //Profile
            $("#Title").text("Profile Overview");
            break;
        case "2": // Settings
            $("#Title").text("Profile Settings");
            break;
        case "3": //  Apps
            $("#Title").text("Connected Apps");
            break;
        case "4": // Bug
            $("#Title").text("Bug Report");
            break;
        case "5": // Help
            $("#Title").text("Help");
            break;
    }

    $('#element_to_pop_up').bPopup();
}

function spammer() {
    $(".spammer").resizable({
        handles: 's'
    });
}
