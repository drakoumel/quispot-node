function init() {
    autoScrollBrowserStop();
    positioning();
    window.scroll();
    autoscroll();
    $(window).on('resize', function () { positioning(); });
}

function positioning() {
    rect = $('.info')[0].getBoundingClientRect();
    if (window.innerHeight < rect.bottom) {
        if (parseInt($('.info').css('margin-top')) > (rect.bottom - window.innerHeight)) {
            nVal = parseInt($('.info').css('margin-top')) - (rect.bottom - window.innerHeight);
            $('.info').css('margin-top', nVal.toString() + 'px');
        }
    }
    else {
        if (parseInt($('.info').css('margin-top')) > (rect.bottom - window.innerHeight)) {
            nVal = parseInt($('.info').css('margin-top')) + (window.innerHeight - rect.bottom);
            if (nVal < 150) {
                $('.info').css('margin-top', nVal.toString() + 'px');
            }
        }
    }
}

function autoscroll() {
    scrollVideoOnce = true;
    scrollStepsOnce = true;

    $(window).on('scroll', function () {
        if (scrollVideoOnce || scrollStepsOnce) {
            rect = $('.video')[0].getBoundingClientRect();
            if (rect.top < window.innerHeight && scrollVideoOnce ) {
                console.log("Ttest");
                $('html, body').animate({
                    scrollTop: $(".video").offset().top - parseInt($('.video').css('margin-top'))
                }, 2000);
                scrollVideoOnce = false;
            }
            else if ($('.steps')[0].getBoundingClientRect().top < window.innerHeight && scrollStepsOnce) {
                //$('html, body').animate({
                //    scrollTop: $(".steps").offset().top - 100
                //}, 2000);
                //scrollStepsOnce = false;
            }
        }
    });

}

function autoScrollBrowserStop() {
    if (window.location.hash) {
        //bind to scroll function
        $(document).scroll(function () {
            var hash = window.location.hash
            var hashName = hash.substring(1, hash.length);
            var element;

            //if element has this id then scroll to it
            if ($(hash).length != 0) {
                element = $(hash);
            }
                //catch cases of links that use anchor name
            else if ($('a[name="' + hashName + '"]').length != 0) {
                //just use the first one in case there are multiples
                element = $('a[name="' + hashName + '"]:first');
            }

            //if we have a target then go to it
            if (element != undefined) {
                window.scrollTo(0, element.position().top);
            }
            //unbind the scroll event
            $(document).unbind("scroll");
        });
    }
}

