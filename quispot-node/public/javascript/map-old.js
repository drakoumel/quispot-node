﻿//-- Map



arrayEvent = new Array();
arrayCategoryOptions = null;
arrayEventType = null;

infowindowZindex = 1;
lastclickmilisecs = 0;
re = /[^0-9]/g;
//===============================================================================
//=================== SETUP =====================================================
//===============================================================================
function totumz() {

    //var lastEventID = 0;

    // Get the HTML for the Event Element.
    //var newEventHtml = '';
    //$.ajax({
    //    type: 'GET',
    //    url: 'html/event.html',
    //    async: false,
    //    success: function (file_html) {
    //        newEventHtml = file_html;
    //    }
    //});

    // Add functionality regarding url location for IE
    //if (!window.location.origin) {
    //    window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
    //}

    // Main map options and Map initialization
    mapOptions = {
        zoom: 10,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        minZoom: 3,
        maxZoom: 23,
        disableDefaultUI: true,
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.BIG,
            position: google.maps.ControlPosition.LEFT_BOTTOM
        }
    };

    //this.addNewPin = function () { console.log(this.map); };

    map = new google.maps.Map(document.getElementById("GoogleMap"), mapOptions);
    //getGeolocationUser(map);
    //google.maps.event.addListener(map, 'rightclick', function () { this.addNewPin() });
    //google.maps.event.addListenerOnce(map, 'idle', function () { RequestData(); });
    //http://gmaps-samples-v3.googlecode.com/svn/trunk/map_events/map_events.html

    // Main functionality for transformable elemtens
    //$(document).dblclick(function (event) {
    //    if ($(event.target).attr('class') !== undefined) {
    //        if ($(event.target).attr('class').indexOf("transformable") !== -1)
    //        { ReplaceByInput($(event.target)); }
    //    }
    //});

    // Beta code for geocode search
    //var input = document.getElementById('searchTextField');
    //var autocomplete = new google.maps.places.Autocomplete(input, {
    //    types: ["geocode"]
    //});
    //autocomplete.bindTo('bounds', map);
    //var place = autocomplete.getPlace();
    //if (place.geometry.viewport) {
    //    map.fitBounds(place.geometry.viewport);
    //} else {
    //    map.setCenter(place.geometry.location);
    //    map.setZoom(17);
    //}

    
}


function RequestData() {
    $.ajax({
        url: window.location.origin + '/events',
        contentType: 'application/json: charset=utf-8',
        dataType: 'json',
        type: 'GET',
        cache: false,
        timeout: 5000,
        async: true,
        success: function (result) {
            result.data.forEach(function (id) { initEvent(new Event(id)); setMaxEventID(id); });
            return true;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('error ' + textStatus + " " + errorThrown);
            return false;
        }
    });
}

function ReplaceByInput(clickedElement) {
    //find clicked element val / inner html
    var clickedElementValue = null;
    if ($(clickedElement).val() != "" && $(clickedElement).val() != undefined) {
        clickedElementValue = $(clickedElement).val();
    }
    else if ($(clickedElement).html() != "" && $(clickedElement).html() != undefined) {
        clickedElementValue = $(clickedElement).html();
    }

    // store the new input element
    var input = $('<input type="text"></input>').addClass("replaceInput");
    $(input).val(clickedElementValue);
    $(clickedElement).replaceWith(input);

    // set cursor and focus to input element
    input[0].selectionStart = input[0].selectionEnd = input.val().length;
    $(input).focus();

    var rereplace = function () {
        $(input).on("blur", function () {
            $(clickedElement).html($(input).val());
            $(input).replaceWith(clickedElement);
        });
    }

    //safety net!
    setTimeout(rereplace, 100);
}

// MARKER WITH PRE EVENT
function addExistingPin(Event) {
    EventGetData(Event);

    google.maps.InfoWindow.prototype.toOpen = true;
    var infowindowcontent;

    $.get('/html/event.html', function (data) { infowindowcontent = data; });

    var infowindow = new google.maps.InfoWindow({
        content: infowindowcontent,
        maxWidth: 500,
        height: 200
    });

    $.get('/html/event.html', function (data) { return data; });

    //var infobox = new InfoBox({
    //    content: ExistingEventBoxHTML(Event),
    //    disableAutoPan: false,
    //    maxWidth: 500,
    //    zIndex: null,
    //    boxStyle: {
    //        background: "url('http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/examples/tipbox.gif') no-repeat",
    //        opacity: 0.75,
    //        width: "280px"
    //    }
    //});


    var marker = new google.maps.Marker({
        map: map,
        draggable: false,
        position: new google.maps.LatLng(Event.getLat(), Event.getLon()),
        icon: Event.getIcon()
    });

    //infobox.open(map, marker);

    google.maps.event.addListener(marker, 'click', function () {
        if (new Date().getTime() - lastclickmilisecs < 1000) {
            lastclickmilisecs = new Date().getTime();
            $('.slowDown').show();
            setTimeout(function () { $('.slowDown').hide(); }, 500);
        }
        else {
            lastclickmilisecs = new Date().getTime();
            if (infowindow.toOpen) {

                infowindow.setZIndex(0);
                infowindow.open(map, marker);
                infowindow.toOpen = false;
            }

            else if (!infowindow.toOpen && infowindow.getZIndex() != 0 && infowindow.getZIndex() < infowindowZindex) {
                infowindow.setZIndex(infowindowZindex);
                infowindow.toOpen = false;
            }
            else if (!infowindow.toOpen) {

                var newEvent = Event // add code for gathering data from infowindow!
                newEvent.setEventName($("#EventName" + Event.getEventID()).html());
                newEvent.setEventDate($("#EventDate" + Event.getEventID()).html());
                newEvent.setDrescription($("#EventDesc" + Event.getEventID()).html());
                newEvent.setPrivacyInt(Event.getPrivacyInt());
                newEvent.setTypeInt(Event.getTypeInt());
                newEvent.setLat(Event.getLat());
                newEvent.setLon(Event.getLon());
                newEvent.setPostCode(Event.getPostCode());
                newEvent.setAddress(Event.getAddress());
                newEvent.setMarketed(false);
                newEvent.setPublisherID(Event.getPublisher());

                if (Save(Event, newEvent)) {
                    infowindow.setContent(ExistingEventBoxHTML(newEvent));
                    infowindow.close();
                    infowindow.toOpen = true;
                }
            }
        }
    });
}

function ExistingEventBoxHTML(Event) {
    var wrapper = $('<div></div>');
    var div = $('<div></div>').addClass("event");
    var deailsDiv = $('<div></div>').addClass("details");
    var extraDeailsDiv = $('<div></div>').addClass("details extra");

    var ul = $('<ul></ul>');
    var li = $('<li></li>');

    deailsDiv.append($('<input type="button" class="vote up"></input>'));
    deailsDiv.append($('<input type="button" class="vote down"></input>'))
            .append($('<intput type="button" Value="" onclick="pinIt(' + Event.getEventID() + ')"/>')
                .addClass("pinit"));

    ul.append($('<li></li>')
        .append($('<h6>' + Event.getEventName() + '</h6>')
            .addClass("transformable")
            .attr("id", "EventName" + Event.getEventID()))
    );

    ul.append($('<li></li>')
        .append($('<label>' + Event.getEventDate() + '</label>')
            .addClass("transformable")
            .attr("id", "EventDate" + Event.getEventID()))
    );

    ul.append($('<li></li>')
        .append($('<p>' + Event.getDescription() + '</p>')
             .addClass("transformable")
            .attr("id", "EventDesc" + Event.getEventID()))
    );

    ul.append($('<li></li>')
        .append($('<span>Privacy</span>')));

    ul.append($('<li></li>')
        .append($('<label>Address:</label><label>' + Event.getAddress() + '</label>')
            .addClass("transformable")));

    deailsDiv.append(ul);
    div.append(deailsDiv);
    wrapper.append(div);

    return wrapper.html();
}

// MARKER WITH NULL EVENT
function addNewPin(cords) {
    map.setCenter(cords);

    lastEventID++
    var newID = lastEventID;

    var newEvent = new Event(-1);
    var contentString = NewEventBoxHTML(newID, newEvent);

    infowindow = new google.maps.InfoWindow({
        content: newEventHtml,
        maxHeight: 500
    });
    console.log(newEventHtml);
    console.log(infowindow.content + "asdas");

    var marker = new google.maps.Marker({
        map: map,
        draggable: false,
        position: new google.maps.LatLng(cords.lat(), cords.lng()),
        icon: '../images/Pins/blueq.png'
    });

    google.maps.event.addListener(marker, 'click', function () {
        if (new Date().getTime() - lastclickmilisecs < 1000) {
            lastclickmilisecs = new Date().getTime();
            $('.slowDown').show();
            setTimeout(function () { $('.slowDown').hide(); }, 500);
        }
        else {
            var goon = true;
            lastclickmilisecs = new Date().getTime();
            marker.setAnimation(google.maps.Animation.BOUNCE);

            console.log(newEvent.getTypeInt());
            if (newEvent.getTypeInt() == -2) {
                console.log("test");
                $('.trafficChoiceBtn' + newID).addClass("attention");
                $('.venueChoiceBtn' + newID).addClass("attention");
                goon = false;
            }

            if ($("#EventName" + newID).val().length <= 0)
            { $("#EventName" + newID).addClass("attention"); goon = false; }

            if ($("#EventDate" + newID).val().length <= 0)
            { $("#EventDate" + newID).addClass("attention"); goon = false; }

            if ($("#EventDesc" + newID).val().length <= 0)
            { $("#EventDesc" + newID).addClass("attention"); goon = false; }

            if (goon) {

                newEvent.setEventName($("#EventName" + newID).val());
                newEvent.setEventDate($("#EventDate" + newID).val());
                newEvent.setDrescription($("#EventDesc" + newID).val());
                newEvent.setPrivacyInt(1);
                newEvent.setLat(cords.lat());
                newEvent.setLon(cords.lng());
                newEvent.setMarketed(false);
                newEvent.setPublisherID(16384);

                if (Save(null, newEvent)) {
                    addExistingPin(newEvent);
                    marker.setAnimation(null); infowindow.close(); marker.setMap(null);
                }
            }
        }
    });
    setTimeout(function () { infowindow.open(map, marker, newID); }, 100);

    google.maps.event.addListener(infowindow, 'domready', function () {
        $('#EventDate' + newID).datepicker({ dateFormat: "D, M dd, yy" });
        $('#EventDate' + newID).on("click", function () { $('#EventDate' + newID).removeClass("attention"); })
        $('#EventName' + newID).on("click", function () { $('#EventName' + newID).removeClass("attention"); })
        $('#EventDesc' + newID).on("click", function () { $('#EventDesc' + newID).removeClass("attention"); })
        $('#EventHour' + newID).change(function () {
            if (re.test($(this).val())) {
                $(this).addClass("attention");
                $(this).val("");
            }
            else {
                $(this).removeClass("attention");
            }
        });
        $('#EventMin' + newID).change(function () {
            if (re.test($(this).val())) {
                $(this).addClass("attention");
                $(this).val("");
            }
            else {
                $(this).removeClass("attention");
            }
        });
    });

    $(document).on("DatePickerShow" + newID, function () {
        $('#ui-datepicker-div').css("z-index", "999");
        $('.datepicker dropdown-menu').css("z-index", "999");
    });

    //Register for Event on InfoWindow Open!
    $(document).on("InfoWinodwOpen" + newID, function () {
        new google.maps.Geocoder().geocode({ 'latLng': cords }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    $('#address' + newID).text(results[0].formatted_address);
                    newEvent.setAddress(results[0].formatted_address);
                }
            } else {
                alert("Geocoder failed due to: " + status);
            }
        });
    });

    $(document).on("VenueClick" + newID, function () {
        arrayEventType.forEach(function (k) { if (k.name === "venue") { newEvent.setTypeInt(k.id); } });
        newEvent.setVenue(true);
        $('#categoryList' + newID).show();
        marker.setIcon(newEvent.getIcon());
        $('.venueChoiceBtn' + newID).removeClass("attention"); $('.trafficChoiceBtn' + newID).removeClass("attention");
    });

    $(document).on("TrafficClick" + newID, function () {
        arrayEventType.forEach(function (k) { if (k.name === "traffic") { newEvent.setTypeInt(k.id); } });
        newEvent.setTraffic(true);
        $('#categoryList' + newID).hide();
        marker.setIcon(newEvent.getIcon());
        $('.trafficChoiceBtn' + newID).removeClass("attention"); $('.venueChoiceBtn' + newID).removeClass("attention");
    });

    $(document).on("catListChange" + newID, function () {
        newEvent.setCategoryID($('#categoryList' + newID + ' option:selected').val());
    });

}

function NewEventBoxHTML(newID) {

    var wrapper = $('<div></div>');
    var div = $('<div></div>').addClass("event new ");
    var deailsDiv = $('<div></div>').addClass("details");
    var extraDeailsDiv = $('<div></div>').addClass("details extra");

    var ul = $('<ul></ul>');
    var venuename = "VenueClick" + newID;
    var trafficname = "TrafficClick" + newID;

    ul.append($('<li></li>')
        .append($('<input type="button"  onclick="triggerEv(\'' + venuename + '\')" Value="Venue" />')
            .addClass("venueChoiceBtn" + newID))
        .append($('<input type="button" onclick="triggerEv(\'' + trafficname + '\')" Value="Traffic" />')
            .addClass("trafficChoiceBtn" + newID))
    );

    ul.append($('<li></li>')
        .append($('<select id="categoryList' + newID + '" onchange="triggerEv(\'' + 'catListChange' + newID + '\')" ></select>')
            .hide()
            .append($.map(arrayCategoryOptions, function (v, k) { return $("<option>").val(k).text(v.name); }))
        )
    );

    ul.append($('<li></li>')
        .append($('<label>Name</label>')
            .addClass("transformable"))
        .append($('<input type="text"></input>')
            .addClass("smallInput")
            .attr("id", "EventName" + newID)
            .css("margin-left", "18px"))
    );

    ul.append($('<li></li>')
        .append($('<label>Date</label>'))
        .append($('<input type="text" readonly="false" onclick="triggerEv(\'' + "DatePickerShow" + newID + '\')"></input>')
            .addClass("smallInput")
            .attr("id", "EventDate" + newID)
            .css("margin-left", "25px"))
    );

    ul.append($('<li></li>')
        .append($('<label style="margin-right:36px;">At</label>'))
        .append($('<input type="text"></input>')
            .addClass("smallerInput")
            .attr("id", "EventHour" + newID))
        .append(":")
        .append($('<input type="text"></input>')
            .addClass("smallerInput")
            .attr("id", "EventMin" + newID))
        .append($('<select id="EventTimeLapse' + newID + '"></select>')
            .append($("<option>").val(1).text("AM"))
            .append($("<option>").val(2).text("PM"))
            .append($("<option>").val(3).text("24"))
        .addClass("smallSelect")
        .attr("id", "EventDate" + newID))
    );

    ul.append($('<li></li>')
        .append($('<p>' + 'Enter Description' + '</p>'))
    );

    ul.append($('<li></li>')
        .append($('<textarea rows="4" cols="21">')
            .attr("id", "EventDesc" + newID))
    );

    ul.append($('<li></li>')
        .append($('<span>Privacy</span>')));

    ul.append($('<li></li>')
        .append($('<label>Enter Address</label>')
            .attr("id", "address" + newID)
            .addClass("address transformable"))
    );


    deailsDiv.append(ul);
    div.append(deailsDiv);
    wrapper.append(div);

    return wrapper.html();
}

function pinIt(newID) {
    console.log("pin it" + newID);
}

function triggerEv(eventName) {
    $.event.trigger({
        type: eventName
    });
}

function getGeolocationUser(map) {
    // Try W3C Geolocation (Preferred)
    if (navigator.geolocation) {
        browserSupportFlag = true;
        navigator.geolocation.getCurrentPosition(function (position) {
            initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            map.setCenter(initialLocation);
        }, function () {
            handleNoGeolocation(browserSupportFlag);
        });
    }
        // Browser doesn't support Geolocation
    else {
        browserSupportFlag = false;
        handleNoGeolocation(browserSupportFlag);
    }

    function handleNoGeolocation(errorFlag) {
        if (errorFlag == true) {
            alert("Geolocation service failed.");
        } else {
            alert("Your browser doesn't support geolocation.");
        }
        map.setCenter(new google.maps.LatLng('46.437857', '-113.466797'));
    }
}

function setMaxEventID(id) {
    if (id > lastEventID)
    { lastEventID = id; }
}

function CategoryDetails() {
    var success = false;
    $.ajax({
        type: "GET",
        url: window.location.origin + '/categoryDetails',
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (result) {
            arrayCategoryOptions = result.data;
            success = true;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('error ' + textStatus + " " + errorThrown);
            success = false;
        }
    });
    return success;
}

function EventTypeDetails() {
    var success = false;
    $.ajax({
        type: "GET",
        url: window.location.origin + '/eventtypeDetails',
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (result) {
            arrayEventType = result.data;
            success = true;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('error ' + textStatus + " " + errorThrown);
            success = false;
        }
    });
    return success;
}

function adjustInfoWinodw() {
    google.maps.InfoWindow.prototype._open = google.maps.InfoWindow.prototype.open;
    google.maps.InfoWindow.prototype._close = google.maps.InfoWindow.prototype.close;
    google.maps.InfoWindow.prototype._openedState = false;

    google.maps.InfoWindow.prototype.open =
    function (map, anchor, ID) {
        this._open(map, anchor);
        $.event.trigger({
            type: "InfoWinodwOpen" + ID,
        });
        this._openedState = true;
    };

    google.maps.InfoWindow.prototype.close =
        function (ID) {
            this._close();
            this._openedState = false;
        };

    google.maps.InfoWindow.prototype.getOpenedState =
        function () {
            return this._openedState;
        };

    google.maps.InfoWindow.prototype.setOpenedState =
        function (val) {
            this._openedState = val;
        };
}
