﻿//-- Map

var map;

//arrayEvent = new Array();
//arrayCategoryOptions = null;
//arrayEventType = null;

//infowindowZindex = 1;
//lastclickmilisecs = 0;
//re = /[^0-9]/g;
//===============================================================================
//=================== SETUP =====================================================
//===============================================================================
function totum() {

    var lastEventID = 0;
    var lastclickmilisecs = 0;
    var infowindowZindex = 1;
    // Get the HTML for the Event Element.
    var newEventHtml = '';
    var existentEventHtml = '';
    $.ajax({
        type: 'GET',
        url: 'html/event.html',
        async: false,
        success: function (file_html) {
            newEventHtml = file_html;
        }
    });

    $.ajax({
        type: 'GET',
        url: 'html/existentEvent.html',
        async: false,
        success: function (file_html) {
            existentEventHtml = file_html;
        }
    });

    // Add functionality regarding url location for IE
    if (!window.location.origin) {
        window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
    }

    // Main map options and Map initialization
    var mapOptions = {
        zoom: 10,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        minZoom: 3,
        maxZoom: 23,
        disableDefaultUI: true,
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.BIG,
            position: google.maps.ControlPosition.LEFT_BOTTOM
        }
    };

    var map = new google.maps.Map(document.getElementById("GoogleMap"), mapOptions);
    getGeolocationUser(map);

    adjustInfoWinodw();

    //GETER FUNCTIONS
    this.getLastEventID = function () { return lastEventID; };


    // ADD PIN FUNCTION
    this.addNewPin = function (e) {
        var gmapCords = new google.maps.LatLng(e.latLng.d, e.latLng.e)
        map.setCenter(gmapCords);
        lastEventID++
        var newID = lastEventID;

        var newEvent = new Event(-1);
        var tempEventHtml = newEventHtml
                        .replace('addressNewID', 'address' + newID.toString())
                        .replace('VenueBtnNewID', 'VenueBtn' + newID.toString())
                        .replace('l1NewID', 'l1' + newID.toString())
                        .replace('l2NewID', 'l2' + newID.toString())
                        .replace('l3NewID', 'l3' + newID.toString())
                        .replace('TrBtnNewID', 'TrBtn' + newID.toString())
                        .replace('PrivBtnNewID', 'PivBtn' + newID.toString())
                        .replace('PubBtnNewID', 'PubBtn' + newID.toString());

        infowindow = new google.maps.InfoWindow({
            content: tempEventHtml,
            maxHeight: 500
        });
        delete tempEventHtml;

        var marker = new google.maps.Marker({
            map: map,
            draggable: false,
            position: gmapCords,
            icon: '../images/Pins/blueq.png'
        });

        //Register Event Bindings
        $(document).on("InfoWindowOpen" + newID, function () {
            new google.maps.Geocoder().geocode({ 'latLng': gmapCords }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        console.log(results[0].formatted_address);
                        $('.address' + newID).val(results[0].formatted_address);
                    }
                } else {
                    alert("Geocoder failed due to: " + status);
                }
            });
        });

        $(document).on("PivBtn" + newID.toString(), function () {
            console.log('test');
        });

        $(document).on("PubBtn" + newID.toString(), function () {
            console.log('test');
        });

        $(document).on("TrBtn" + newID.toString(), function () {
            console.log('test');
        });

        $(document).on("l1" + newID.toString(), function () {
            console.log('test');
        });
        $(document).on("l2" + newID.toString(), function () {
            console.log('test');
        });
        $(document).on("l3" + newID.toString(), function () {
            console.log('test');
            //arrayEventType.forEach(function (k) { if (k.name === "venue") { newEvent.setTypeInt(k.id); } });
            //newEvent.setVenue(true);
            //$('#categoryList' + newID).show();
            //marker.setIcon(newEvent.getIcon());
            //$('.venueChoiceBtn' + newID).removeClass("attention"); $('.trafficChoiceBtn' + newID).removeClass("attention");
        });

        google.maps.event.addListener(marker, 'click', function () {
            if (new Date().getTime() - lastclickmilisecs < 1000) {
                lastclickmilisecs = new Date().getTime();
                console.log('slow down');
            }
            else {
                lastclickmilisecs = new Date().getTime();
                marker.setAnimation(google.maps.Animation.BOUNCE);
                Save(null, newEvent)
                // try to save, if u can save it stop bouncing yo
                //marker.setAnimation(null); infowindow.close(); marker.setMap(null);

            }
        })

        //After all is set Open the marker / infowindow
        infowindow.open(map, marker, newID);
    }

    this.addPinExistentEvent = function (id) {
        setMaxEventID(id, lastEventID);
        console.log(id);
        var innerEvent = new Event(id);

        $.ajax({
            type: "POST",
            url: window.location.origin + '/getEventDetails',
            contentType: 'application/json',
            dataType: 'json',
            async: true,
            data: JSON.stringify({ id: innerEvent.getEventID() }),
            success: function (result) {

                var result = result[0];
                innerEvent.setEventName(result.EventName);
                innerEvent.setPrivacyInt(result.PrivacyLevel);
                innerEvent.setTypeInt(result.TypeInt);
                innerEvent.setAddress(result.Address);
                innerEvent.setPostCode(result.PostCode);
                innerEvent.setDrescription(result.Desc);
                innerEvent.setDownVote(result.Down);
                innerEvent.setUpVote(result.Up);
                innerEvent.setEventDate(result.ExpectedDate);
                innerEvent.setLat(result.Lat);
                innerEvent.setLon(result.Lon);
                innerEvent.setMarketed(result.Marketed);
                innerEvent.setCategoryID(result.setCategoryID);
                innerEvent.setPublisherID(result.setPublisherID);

                console.log(innerEvent.toJSONString());

                var gmapCords = new google.maps.LatLng(innerEvent.getLat(), innerEvent.getLon())
                var tempExistentEventHtml = existentEventHtml;
                tempExistentEventHtml = tempExistentEventHtml
                                        .replace('VenueBtnNoID', 'VenueBtn' + innerEvent.getEventID().toString())
                                        .replace('TrafficBtnNoID', 'TrafficBtn' + innerEvent.getEventID().toString())
                                        .replace('PinNoID', 'Pin' + innerEvent.getEventID().toString())
                                        .replace('EventNameNOID', 'EventName' + innerEvent.getEventID().toString())
                                        .replace('EventDateNOID', 'EventDate' + innerEvent.getEventID().toString())
                                        .replace('EventDescNOID', 'EventDesc' + innerEvent.getEventID().toString())
                                        .replace('EventAddNOID', 'EventAdd' + innerEvent.getEventID().toString())
                                        .replace('PrivBtnNOID', 'PrivBtn' + innerEvent.getEventID().toString())
                                        .replace('PublicBtnNOID', 'PublicBtn' + innerEvent.getEventID().toString())
                                        .replace('AttendBtnNOID', 'AttendBtn' + innerEvent.getEventID().toString())
                                        .replace('EventNameNOIDVal', innerEvent.getEventName().toString())
                                        .replace('EventDateNOIDVal', innerEvent.getEventDate().toString())
                                        .replace('EventDescNOIDVal', innerEvent.getDescription().toString())
                                        .replace('EventAddNOIDVal', innerEvent.getAddress().toString());

                infowindow = new google.maps.InfoWindow({
                    content: tempExistentEventHtml,
                    maxHeight: 500
                });
                delete tempExistentEventHtml;

                var marker = new google.maps.Marker({
                    map: map,
                    draggable: false,
                    position: gmapCords,
                    icon: '../images/Pins/blueq.png'
                });

                google.maps.event.addListener(this.marker, 'click', function () {
                    if (new Date().getTime() - lastclickmilisecs < 10000) {
                        lastclickmilisecs = new Date().getTime();
                        //$('.slowDown').show();
                        //setTimeout(function () { $('.slowDown').hide(); }, 500);
                        console.log("slow down");
                    }
                    else {
                        lastclickmilisecs = new Date().getTime();
                        console.log(infowindow.getZIndex());
                        if (!infowindow.getOpenedState()) {
                            infowindow.setZIndex(0);
                            infowindow.open(map, marker);
                            infowindow.setOpenedState(true);
                        }
                        else if (infowindow.getOpenedState() && infowindow.getZIndex() != 0 && infowindow.getZIndex() < infowindowZindex) {
                            infowindow.setZIndex(infowindowZindex);
                            infowindow.setOpenedState(true);
                        }
                        else if (infowindow.getOpenedState()) {
                            var newEvent = innerEvent; // add code for gathering data from infowindow!
                            newEvent.setEventName($("#EventName" + innerEvent.getEventID()).text());
                            newEvent.setEventDate($("#EventDate" + innerEvent.getEventID()).text());
                            newEvent.setDrescription($("#EventDesc" + innerEvent.getEventID()).text());
                            newEvent.setAddress($("#EventAdd" + innerEvent.getEventID()).text());

                            if (Save(Event, newEvent)) {
                                infowindow.close();
                                infowindow.setOpenedState(false);
                            }
                        }
                    }
                })

                //infowindow.open(map, marker);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('error ' + textStatus + " " + errorThrown);
                success = false;
            }
        });


    }

    // REGISTER RIGHT CLICK TO ADD PIN FUNCTION
    google.maps.event.addListener(map, 'rightclick', function (e) { addNewPin(e); });
    // On map load, load all the events
    google.maps.event.addListenerOnce(map, 'idle', function () { RequestData(map, existentEventHtml); });
    //http://gmaps-samples-v3.googlecode.com/svn/trunk/map_events/map_events.html

    // Main functionality for transformable elemtens
    $(document).dblclick(function (event) {
        if ($(event.target).attr('class') !== undefined) {
            if ($(event.target).attr('class').indexOf("transformable") !== -1)
            { ReplaceByInput($(event.target)); }
        }
    });
}

function RequestData(map, existentEventHtml) {
    console.log('test1');
    $.ajax({
        url: window.location.origin + '/events',
        contentType: 'application/json: charset=utf-8',
        dataType: 'json',
        type: 'GET',
        cache: false,
        timeout: 5000,
        async: true,
        success: function (result) {
            result.data.forEach(function (id) { addPinExistentEvent(id); });
            return true;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('error ' + textStatus + " " + errorThrown);
            return false;
        }
    });
}

function ReplaceByInput(clickedElement) {
    //find clicked element val / inner html
    var clickedElementValue = null;
    if ($(clickedElement).val() != "" && $(clickedElement).val() != undefined) {
        clickedElementValue = $(clickedElement).val();
    }
    else if ($(clickedElement).html() != "" && $(clickedElement).html() != undefined) {
        clickedElementValue = $(clickedElement).html();
    }

    // store the new input element
    var input = $('<input type="text"></input>').addClass("replaceInput");
    $(input).val(clickedElementValue);
    $(clickedElement).replaceWith(input);

    // set cursor and focus to input element
    input[0].selectionStart = input[0].selectionEnd = input.val().length;
    $(input).focus();

    var rereplace = function () {
        $(input).on("blur", function () {
            $(clickedElement).html($(input).val());
            $(input).replaceWith(clickedElement);
        });
    }

    //safety net!
    setTimeout(rereplace, 100);
}

//// MARKER WITH PRE EVENT
//function addExistingPin(Event) {
//    EventGetData(Event);

//    google.maps.InfoWindow.prototype.toOpen = true;
//    var infowindowcontent;

//    $.get('/html/event.html', function (data) { infowindowcontent = data; });

//    var infowindow = new google.maps.InfoWindow({
//        content: infowindowcontent,
//        maxWidth: 500,
//        height: 200
//    });

//    $.get('/html/event.html', function (data) { return data; });

//    //var infobox = new InfoBox({
//    //    content: ExistingEventBoxHTML(Event),
//    //    disableAutoPan: false,
//    //    maxWidth: 500,
//    //    zIndex: null,
//    //    boxStyle: {
//    //        background: "url('http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/examples/tipbox.gif') no-repeat",
//    //        opacity: 0.75,
//    //        width: "280px"
//    //    }
//    //});


//    var marker = new google.maps.Marker({
//        map: map,
//        draggable: false,
//        position: new google.maps.LatLng(Event.getLat(), Event.getLon()),
//        icon: Event.getIcon()
//    });

//    //infobox.open(map, marker);

//    google.maps.event.addListener(marker, 'click', function () {
//        if (new Date().getTime() - lastclickmilisecs < 1000) {
//            lastclickmilisecs = new Date().getTime();
//            $('.slowDown').show();
//            setTimeout(function () { $('.slowDown').hide(); }, 500);
//        }
//        else {
//            lastclickmilisecs = new Date().getTime();
//            if (infowindow.toOpen) {

//                infowindow.setZIndex(0);
//                infowindow.open(map, marker);
//                infowindow.toOpen = false;
//            }

//            else if (!infowindow.toOpen && infowindow.getZIndex() != 0 && infowindow.getZIndex() < infowindowZindex) {
//                infowindow.setZIndex(infowindowZindex);
//                infowindow.toOpen = false;
//            }
//            else if (!infowindow.toOpen) {

//                var newEvent = Event // add code for gathering data from infowindow!
//                newEvent.setEventName($("#EventName" + Event.getEventID()).html());
//                newEvent.setEventDate($("#EventDate" + Event.getEventID()).html());
//                newEvent.setDrescription($("#EventDesc" + Event.getEventID()).html());
//                newEvent.setPrivacyInt(Event.getPrivacyInt());
//                newEvent.setTypeInt(Event.getTypeInt());
//                newEvent.setLat(Event.getLat());
//                newEvent.setLon(Event.getLon());
//                newEvent.setPostCode(Event.getPostCode());
//                newEvent.setAddress(Event.getAddress());
//                newEvent.setMarketed(false);
//                newEvent.setPublisherID(Event.getPublisher());

//                if (Save(Event, newEvent)) {
//                    infowindow.setContent(ExistingEventBoxHTML(newEvent));
//                    infowindow.close();
//                    infowindow.toOpen = true;
//                }
//            }
//        }
//    });
//}

//function ExistingEventBoxHTML(Event) {
//    var wrapper = $('<div></div>');
//    var div = $('<div></div>').addClass("event");
//    var deailsDiv = $('<div></div>').addClass("details");
//    var extraDeailsDiv = $('<div></div>').addClass("details extra");

//    var ul = $('<ul></ul>');
//    var li = $('<li></li>');

//    deailsDiv.append($('<input type="button" class="vote up"></input>'));
//    deailsDiv.append($('<input type="button" class="vote down"></input>'))
//            .append($('<intput type="button" Value="" onclick="pinIt(' + Event.getEventID() + ')"/>')
//                .addClass("pinit"));

//    ul.append($('<li></li>')
//        .append($('<h6>' + Event.getEventName() + '</h6>')
//            .addClass("transformable")
//            .attr("id", "EventName" + Event.getEventID()))
//    );

//    ul.append($('<li></li>')
//        .append($('<label>' + Event.getEventDate() + '</label>')
//            .addClass("transformable")
//            .attr("id", "EventDate" + Event.getEventID()))
//    );

//    ul.append($('<li></li>')
//        .append($('<p>' + Event.getDescription() + '</p>')
//             .addClass("transformable")
//            .attr("id", "EventDesc" + Event.getEventID()))
//    );

//    ul.append($('<li></li>')
//        .append($('<span>Privacy</span>')));

//    ul.append($('<li></li>')
//        .append($('<label>Address:</label><label>' + Event.getAddress() + '</label>')
//            .addClass("transformable")));

//    deailsDiv.append(ul);
//    div.append(deailsDiv);
//    wrapper.append(div);

//    return wrapper.html();
//}

//// MARKER WITH NULL EVENT


//function NewEventBoxHTML(newID) {

//    var wrapper = $('<div></div>');
//    var div = $('<div></div>').addClass("event new ");
//    var deailsDiv = $('<div></div>').addClass("details");
//    var extraDeailsDiv = $('<div></div>').addClass("details extra");

//    var ul = $('<ul></ul>');
//    var venuename = "VenueClick" + newID;
//    var trafficname = "TrafficClick" + newID;

//    ul.append($('<li></li>')
//        .append($('<input type="button"  onclick="triggerEv(\'' + venuename + '\')" Value="Venue" />')
//            .addClass("venueChoiceBtn" + newID))
//        .append($('<input type="button" onclick="triggerEv(\'' + trafficname + '\')" Value="Traffic" />')
//            .addClass("trafficChoiceBtn" + newID))
//    );

//    ul.append($('<li></li>')
//        .append($('<select id="categoryList' + newID + '" onchange="triggerEv(\'' + 'catListChange' + newID + '\')" ></select>')
//            .hide()
//            .append($.map(arrayCategoryOptions, function (v, k) { return $("<option>").val(k).text(v.name); }))
//        )
//    );

//    ul.append($('<li></li>')
//        .append($('<label>Name</label>')
//            .addClass("transformable"))
//        .append($('<input type="text"></input>')
//            .addClass("smallInput")
//            .attr("id", "EventName" + newID)
//            .css("margin-left", "18px"))
//    );

//    ul.append($('<li></li>')
//        .append($('<label>Date</label>'))
//        .append($('<input type="text" readonly="false" onclick="triggerEv(\'' + "DatePickerShow" + newID + '\')"></input>')
//            .addClass("smallInput")
//            .attr("id", "EventDate" + newID)
//            .css("margin-left", "25px"))
//    );

//    ul.append($('<li></li>')
//        .append($('<label style="margin-right:36px;">At</label>'))
//        .append($('<input type="text"></input>')
//            .addClass("smallerInput")
//            .attr("id", "EventHour" + newID))
//        .append(":")
//        .append($('<input type="text"></input>')
//            .addClass("smallerInput")
//            .attr("id", "EventMin" + newID))
//        .append($('<select id="EventTimeLapse' + newID + '"></select>')
//            .append($("<option>").val(1).text("AM"))
//            .append($("<option>").val(2).text("PM"))
//            .append($("<option>").val(3).text("24"))
//        .addClass("smallSelect")
//        .attr("id", "EventDate" + newID))
//    );

//    ul.append($('<li></li>')
//        .append($('<p>' + 'Enter Description' + '</p>'))
//    );

//    ul.append($('<li></li>')
//        .append($('<textarea rows="4" cols="21">')
//            .attr("id", "EventDesc" + newID))
//    );

//    ul.append($('<li></li>')
//        .append($('<span>Privacy</span>')));

//    ul.append($('<li></li>')
//        .append($('<label>Enter Address</label>')
//            .attr("id", "address" + newID)
//            .addClass("address transformable"))
//    );


//    deailsDiv.append(ul);
//    div.append(deailsDiv);
//    wrapper.append(div);

//    return wrapper.html();
//}

//function pinIt(newID) {
//    console.log("pin it" + newID);
//}

function triggerEv(eventName) {
    $.event.trigger({
        type: eventName
    });
}

function getGeolocationUser(map) {
    // Try W3C Geolocation (Preferred)
    if (navigator.geolocation) {
        browserSupportFlag = true;
        navigator.geolocation.getCurrentPosition(function (position) {
            initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            map.setCenter(initialLocation);
        }, function () {
            handleNoGeolocation(browserSupportFlag);
        });
    }
        // Browser doesn't support Geolocation
    else {
        browserSupportFlag = false;
        handleNoGeolocation(browserSupportFlag);
    }
}

function handleNoGeolocation(errorFlag) {
    if (errorFlag == true) {
        alert("Geolocation service failed.");
    } else {
        alert("Your browser doesn't support geolocation.");
    }
    map.setCenter(new google.maps.LatLng('46.437857', '-113.466797'));
}


function setMaxEventID(id, lastEventID) {
    if (id > lastEventID)
    { lastEventID = id; }
}

function CategoryDetails() {
    var success = false;
    $.ajax({
        type: "GET",
        url: window.location.origin + '/categoryDetails',
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (result) {
            arrayCategoryOptions = result.data;
            success = true;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('error ' + textStatus + " " + errorThrown);
            success = false;
        }
    });
    return success;
}

function EventTypeDetails() {
    var success = false;
    $.ajax({
        type: "GET",
        url: window.location.origin + '/eventtypeDetails',
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: function (result) {
            arrayEventType = result.data;
            success = true;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('error ' + textStatus + " " + errorThrown);
            success = false;
        }
    });
    return success;
}

function adjustInfoWinodw() {
    google.maps.InfoWindow.prototype._open = google.maps.InfoWindow.prototype.open;
    google.maps.InfoWindow.prototype._close = google.maps.InfoWindow.prototype.close;
    google.maps.InfoWindow.prototype._openedState = false;

    google.maps.InfoWindow.prototype.open =
    function (map, anchor, ID) {
        this._open(map, anchor);
        $.event.trigger({
            type: "InfoWindowOpen" + ID,
        });
        this._openedState = true;
    };

    google.maps.InfoWindow.prototype.close =
        function (ID) {
            this._close();
            this._openedState = false;
        };

    google.maps.InfoWindow.prototype.getOpenedState =
        function () {
            return this._openedState;
        };

    google.maps.InfoWindow.prototype.setOpenedState =
        function (val) {
            this._openedState = val;
        };
}
