
function init() {
    if (!window.location.origin) {
        window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
    }

    var mainactivity = '';
    var secondaryactivity = '';
    var createEventBtnClicked = false;
    var tabflicker = null;
    var spot = null;

    newEvent = new Event();

    $('#boardgames').click(function () {
        newEvent.setActivity('b');
        $('#act1-sub-title').html("Pick a board game!");
        if (!createEventBtnClicked) {
            $('.boardgames').fadeIn(); $('.sports').hide(); $('.cardgames').hide(); mainactivity = 'b';
        }
    });
    $('#cardgames').click(function () {
        newEvent.setActivity('c');
        $('#act1-sub-title').html("Pick a card game!");
        if (!createEventBtnClicked) {
            $('.cardgames').fadeIn(); $('.sports').hide(); $('.boardgames').hide(); mainactivity = 'c';
        }
    });
    $('#sports').click(function () {
        newEvent.setActivity('s');
        $('#act1-sub-title').html("Pick a sport!");
        if (!createEventBtnClicked) {
            $('.sports').fadeIn(); $('.cardgames').hide(); $('.boardgames').hide(); mainactivity = 's';
        }
    });

    $('.sports').change(function () { secondaryactivity = this.value; newEvent.setSubActivity(secondaryactivity); $('#tab2').click() });
    $('.cardgames').change(function () { secondaryactivity = this.value; newEvent.setSubActivity(secondaryactivity); $('#tab2').click() });
    $('.boardgames').change(function () { secondaryactivity = this.value; newEvent.setSubActivity(secondaryactivity); $('#tab2').click() });

    $('#datetimepicker')
    .datetimepicker()
    .on('hide', function () {
        window.setTimeout(function () {
            if ($('#tab2').parent().hasClass("active") && $('.act2-date').val().length > 0) {
                console.log($('#mirror_field_act2').val());
                newEvent.setDatetime($('#mirror_field_act2').val());
                $('#tab3').click();
            }
        }, 500)
    });

    $('#tab3').click(function () {
        Map(); RequestSpot();
    });

    $('#pickspot').click(function () {
        // SET THIS FROM PIN CLICK.
        if (spot !== null || spot !== undefined) {
            newEvent.setSpot(spot);
            $('#tab4').click();
        }
    });

    $(".act4-numbers").change(function () {
        newEvent.setHowMany($('.act4-numbers').val());
        if ($('.act4-numbers').val().length > 0) {
            tabflicker = window.setInterval(function () {
                $('#tab5').parent().toggleClass('white');
            }, 700);
        }
    });


    $('#tab5').click(function () {
        window.clearInterval(tabflicker);
        $('#tab5').parent().removeClass('white');
        if (newEvent.isOk()) {
            if ($('.create').is(':disabled')) {
                $('.create').removeAttr("disabled", "disabled");
            }

            $('#hdnSub').val(newEvent.getSubActivity());
            $('#hdnSpotID').val(newEvent.getSpot()._id);
            $('#hdnDate').val(newEvent.getDatetime());
            $('#hdnMany').val(newEvent.getHowMany());

            $('#reviewWhat').text("You are gonna play " + newEvent.getSubActivity());
            $('#reviewWhen').text("At " + newEvent.getDatetime());
            $('#reviewWhere').text(newEvent.getSpot().name);
            $('#reviewWho').text("And you need " + newEvent.getHowMany() + " more.");

            $('#reviewWhat').click(function () { $('#tab1').click(); });
            $('#reviewWhen').click(function () { $('#tab2').click(); });
            $('#reviewWhere').click(function () { $('#tab3').click(); });
            $('#reviewWho').click(function () { $('#tab4').click(); });
        }
        else {
            $('#reviewWhat').text("Please complete the previous steps before creating an event");
        }
    });

    function RequestSpot() {
        console.log('requested spot', mainactivity);
        $.ajax({
            type: "POST",
            url: window.location.origin + '/spots',
            contentType: 'application/json',
            dataType: 'json',
            async: true,
            data: JSON.stringify({ mainactivity: mainactivity, activity: secondaryactivity }),
            success: function (result) {
                PopulateSpotMap(result.result);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('error ' + textStatus + " " + errorThrown);
                success = false;
            }
        });
    }

    function PopulateSpotMap(Spot) {
        for (var i = 0; i < Spot.length; i++) {
            console.log(Spot[i]);
            addNewPin(Spot[i])
        }
    }

    var map;
    function Map() {
        var mapOptions = {
            zoom: 10,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            minZoom: 3,
            maxZoom: 23,
            disableDefaultUI: true,
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.BIG,
                position: google.maps.ControlPosition.LEFT_BOTTOM
            }
        };

        // Try W3C Geolocation (Preferred)
        if (navigator.geolocation) {
            browserSupportFlag = true;
            navigator.geolocation.getCurrentPosition(function (position) {
                initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                map.setCenter(initialLocation);
            }, function () {
                handleNoGeolocation(browserSupportFlag);
            });
        }
            // Browser doesn't support Geolocation
        else {
            browserSupportFlag = false;
            handleNoGeolocation(browserSupportFlag);
        }

        map = new google.maps.Map(document.getElementById("GoogleMap"), mapOptions);
    }

    this.addNewPin = function (e) {
        var gmapCords = new google.maps.LatLng(e.lat, parseFloat(e.lon));
        console.log(gmapCords);
        map.setCenter(gmapCords);

        var marker = new google.maps.Marker({
            map: map,
            draggable: false,
            position: gmapCords
        });

        infowindow = new google.maps.InfoWindow({
            content: e.name,
            maxHeight: 500
        });

        google.maps.event.addListener(marker, 'click', function () {
            spot = e;
            infowindow.open(map, marker);
            LoadMiniSpot(mainactivity, e.name);
        });
    }
}

function LoadMiniSpot(label, name) {
    $('#miniSpot').show();
    $('#mainactivitylabel').text("spot");
    $('#mainactivityname').text(name);
}



function Event() {
    var activity = null;
    var subAcitivity = null;
    var datetime = null;
    var spotid = null;
    var howmany = 0;
    var friends = null;

    this.getActivity = function () { return activity; }
    this.getSubActivity = function () { return subAcitivity; }
    this.getDatetime = function () { return datetime; }
    this.getSpot = function () { return spot; }
    this.getHowMany = function () { return howmany; }
    this.getFriends = function () { return friends; }

    this.isOk = function () {
        if (activity && subAcitivity && datetime && spot && howmany != null) {
            return true;
        }
        else return false;
    }

    this.setActivity = function (nVal) { activity = nVal; }
    this.setSubActivity = function (nVal) { subAcitivity = nVal; }
    this.setDatetime = function (nVal) { datetime = nVal; }
    this.setSpot = function (nVal) { spot = nVal; }
    this.setHowMany = function (nVal) { howmany = nVal; }
    this.setFriends = function (nVal) { friends = nVal; }
}



